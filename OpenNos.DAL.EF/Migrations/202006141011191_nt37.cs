﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nt37 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CharacterSkill", "TattooBuff", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CharacterSkill", "TattooBuff");
        }
    }
}
